--Write statement(s) to return all ‘shop_new’ activities in the system. Please include the client name and whether the client is active as part of the results.
SELECT clients.name as client,
  clients.active as active,
  activity_type.name as activity,
  activity_log.activity_date
FROM activity_log
JOIN clients on activity_log.client_id=clients.id
JOIN activity_type on activity_log.activity_type_id=activity_type.id
WHERE activity_type_id=1;
