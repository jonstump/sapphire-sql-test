--Provide the statement(s) necessary to show a count of each activity type by client. Please exclude inactive clients within these results.
SELECT clients.name as client,
  activity_type.name as activity,
  COUNT(activity_type_id)
FROM activity_log
JOIN activity_type ON activity_log.activity_type_id=activity_type.id
JOIN clients ON activity_log.client_id=clients.id
WHERE clients.active = true
GROUP BY clients.name,
  activity_type.name
ORDER BY clients.name;
