--Write a query or queries to return any activity for any client that took place in an odd-numbered month. Please include client name and activity name in the results.
SELECT clients.name as client,
  activity_type.name as activity,
  date_part('month', activity_log.activity_date) as date
FROM activity_log
JOIN clients ON clients.id=activity_log.client_id
JOIN activity_type ON activity_log.activity_type_id=activity_type.id
WHERE date_part('month',activity_log.activity_date)::INTEGER % 2 <> 0;
