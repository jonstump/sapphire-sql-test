This repository represents work for a SQL Technical Assessment for Sapphire Digital

I hope the code reviewer has as much fun with my work as I did making it. Thank you for your time and consideration.

Each numbered SQL file corresponds with the numbered assessment question. example: 2.sql is the answer for question two

1. The answers below assume I am working with the following RDBMS
system:

PostgreSQL 12.8 on Ubuntu 20.04
A dump file of my database is included

All queries are attached as well and git tracked so you can look through my prior psuedo code and updates to the final polish as well. Below are the outputs I received when I ran my query locally on my PostgreSQL server.

2. Write a SQL statement that returns the count of active clients in the system

![2sqlscreenshot](/screenshots/2sql.png)

3. Write statement(s) to return all ‘shop_new’ activities in the system.  Please include the
client name and whether the client is active as part of the results.

![3sqlscreenshot](/screenshots/3sql.png)

4. Provide the statement(s) necessary to show a count of each activity type by client.
Please exclude inactive clients within these results.

![4sqlscreenshot](/screenshots/4sql.png)

5. Write a query or queries to return any activity for any client that took place in an
odd-numbered month.  Please include client name and activity name in the results.

![5sqlscreenshot](/screenshots/5sql.png)

I did make one assumption while making these queries and that was optimization for humans rather than machines. It might make more sense to break these up, but I also like the idea of keeping queries that others with less SQL knowledge can use if need be.
